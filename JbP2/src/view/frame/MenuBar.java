package view.frame;

import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import controller.EnumMenu.MenuItem;
import view.listener.EventListener;
import controller.PresentationInterface;


public class MenuBar {

  public JMenuBar createMenuBar(SliderFrame sf, PresentationInterface controller) {
    JMenuBar menuBar = new JMenuBar();
    EventListener endingListener = new EventListener(sf,controller);

    JMenu fileMenu = new JMenu(MenuItem.FILE.menuTextItem);
    JMenuItem fileOpenItem = new JMenuItem(MenuItem.OPEN.menuTextItem);
    fileOpenItem.addActionListener(endingListener);

    JMenuItem fileLoadItem = new JMenuItem(MenuItem.NEW.menuTextItem);
    fileLoadItem.addActionListener(endingListener);

    JMenuItem fileSaveItem = new JMenuItem(MenuItem.SAVE.menuTextItem);
    fileSaveItem.addActionListener(endingListener);

    JMenuItem fileExitItem = new JMenuItem(MenuItem.EXIT.menuTextItem);
    fileExitItem.addActionListener(endingListener);

    fileMenu.add(fileOpenItem);
    fileMenu.add(fileLoadItem);
    fileMenu.add(fileSaveItem);
    fileMenu.addSeparator();
    fileMenu.add(fileExitItem);

    JMenu viewMenu = new JMenu(MenuItem.VIEW.menuTextItem);
    JMenuItem viewNextItem = new JMenuItem(MenuItem.NEXT.menuTextItem, KeyEvent.VK_PAGE_DOWN);
    viewNextItem.addActionListener(endingListener);

    JMenuItem viewPrevItem = new JMenuItem(MenuItem.PREV.menuTextItem, KeyEvent.VK_PAGE_UP);
    viewPrevItem.addActionListener(endingListener);

    JMenuItem viewGoToItem = new JMenuItem(MenuItem.GOTO.menuTextItem);
    viewGoToItem.addActionListener(endingListener);

    viewMenu.add(viewNextItem);
    viewMenu.add(viewPrevItem);
    viewMenu.add(viewGoToItem);

    JMenu helpMenu = new JMenu(MenuItem.HELP.menuTextItem);
    JMenuItem helpAboutItem = new JMenuItem(MenuItem.ABOUT.menuTextItem);
    helpAboutItem.addActionListener(endingListener);

    helpMenu.add(helpAboutItem);

    menuBar.add(fileMenu);
    menuBar.add(viewMenu);
    menuBar.add(helpMenu);
   
    return menuBar;

  }

}
