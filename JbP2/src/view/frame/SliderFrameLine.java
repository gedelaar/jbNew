package view.frame;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import view.style.Image;
import view.style.ItemStyle;
import view.style.Line;

/**
 * @author gerard/marcel
 * 
 *         setup a single line on the frame a line contains text or an image in
 *         the right style
 *
 */
public class SliderFrameLine {
  private static final String imagePath = "images/";

  public Document setLineStyle(Line line, Document document) {
    SimpleAttributeSet style = addStyle(line.getStyle().build());
    ((StyledDocument) document).setParagraphAttributes(document.getEndPosition().getOffset() - 1,
	document.getLength() - 1, style, true);
    try {
      int i = document.getEndPosition().getOffset() - 1;
      document.insertString(i, line.getStrLine() + "\n", style);
    }
    catch (BadLocationException e) {
      e.printStackTrace();
    }
    return document;
  }

  public Document setImageStyle(Image image, Document document) {
    StyleContext context = new StyleContext();
    Style labelStyle = context.getStyle(StyleContext.DEFAULT_STYLE);
    SimpleAttributeSet style = addStyle(image.getStyle().build());
    ((StyledDocument) document).setParagraphAttributes(document.getEndPosition().getOffset() - 1,
	document.getLength() - 1, style, true);
    JLabel label = new JLabel(new ImageIcon(imagePath + image.getLine()));
    StyleConstants.setComponent(labelStyle, label);
    try {
      int i = document.getEndPosition().getOffset() - 1;
      document.insertString(i, "\n", labelStyle);
    }
    catch (BadLocationException e) {
      e.printStackTrace();
    }
    return document;

  }

  private SimpleAttributeSet addStyle(ItemStyle itemStyle) {
    final SimpleAttributeSet style = new SimpleAttributeSet();
    if (itemStyle.getPosition() != null && itemStyle.getPosition().equals("right")) {
      StyleConstants.setAlignment(style, StyleConstants.ALIGN_RIGHT);
    }
    else {
      StyleConstants.setAlignment(style, StyleConstants.ALIGN_LEFT);
    }
    StyleConstants.setFontSize(style, itemStyle.getFontSize());
    StyleConstants.setSpaceAbove(style, 4);
    StyleConstants.setSpaceBelow(style, 4);
    StyleConstants.setForeground(style, itemStyle.getColor());
    StyleConstants.setFontFamily(style, itemStyle.getFont());
    StyleConstants.setLeftIndent(style, itemStyle.getIndent());
    return style;
  }
}
