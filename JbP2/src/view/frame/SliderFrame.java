package view.frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.Serializable;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.text.Document;

import controller.PresentationInterface;
import model.item.ImageItem;
import model.item.SlideItem;
import model.item.TextItem;
import view.listener.KeyController;
import view.style.Line;
import view.style.PreSetStyle;

/**
 * @author gerard/Marcel
 * 
 *         setup total frame for presenting the content
 *
 */
public class SliderFrame extends JFrame implements Serializable {

  public static final int WIDTH = 900;
  public static final int HEIGHT = 800;

  private static final long serialVersionUID = 1L;

  private PresentationInterface controller;
  private Document document;
  private JTextArea textArea;

  public SliderFrame(PresentationInterface controller) {
    super();
    this.controller = controller;
    textArea = this.SliderFrameTextArea();
    this.slideSetup();
  }

  private JTextArea SliderFrameTextArea() {
    textArea = new JTextArea();
    textArea.setEditable(false);
    textArea.addKeyListener(new KeyController(this, controller));
    add(textArea, BorderLayout.CENTER);
    return textArea;
  }

  private void addMenuBar() {
    setJMenuBar(new MenuBar().createMenuBar(this, controller));
  }

  private void slideSetup() {
    this.addMenuBar();
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setSize(WIDTH, HEIGHT);
    setLocationRelativeTo(null);
    setVisible(true);
  }

  private void slideSetupHeader() {
    String curSlideNumber = String.valueOf(controller.getSlideShowIterator().getCurrentSlideNumber());
    String maxSlideNumber = String.valueOf(controller.getSlideShowIterator().getMaxSlideNumber());
    Line header = new Line("Slide " + curSlideNumber + " of " + maxSlideNumber);
    header.setStyle(new PreSetStyle().getStyle(PreSetStyle.HEADER));
    document = new SliderFrameLine().setLineStyle(header, document);
  }

  public void addPaneList(List<SlideItem> slideItems) {
    JTextPane textPane = new JTextPane();
    textPane.setPreferredSize(new Dimension(WIDTH, HEIGHT));
    document = textPane.getStyledDocument();
    this.getContentPane().add(textPane, BorderLayout.CENTER);
    this.slideSetupHeader();
    for (SlideItem slideItem : slideItems) {
      if (this.isTextItem(slideItem)) {
	Line line = slideItem.getLine();
	document = new SliderFrameLine().setLineStyle(line, document);
      }
      if (this.isImageItem(slideItem)) {
	view.style.Image image = ((ImageItem) slideItem).getImage();
	document = new SliderFrameLine().setImageStyle(image, document);
      }
    }
  }

  private boolean isTextItem(SlideItem slideItem) {
    return (slideItem instanceof TextItem);
  }

  private boolean isImageItem(SlideItem slideItem) {
    return slideItem instanceof ImageItem;
  }

}
