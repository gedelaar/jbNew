package view.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controller.PresentationInterface;
import view.frame.SliderFrame;
import controller.EnumMenu;
import controller.EnumMenu.MenuItem;

public class EventListener implements ActionListener {
  private SliderFrame frame;
  private PresentationInterface controller;

  public EventListener(SliderFrame frame, PresentationInterface controller) {
    this.frame = frame;
    this.controller = controller;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    MenuItem menuItem = EnumMenu.MenuItem.valueOf(e.getActionCommand().toUpperCase());

    switch (menuItem) {
      case OPEN:
	this.openSlideShow();
	break;
      case NEW:
	this.loadSlideShow();
	break;
      case NEXT:
	this.nextSlide();
	break;
      case PREV:
	this.prevSlide();
	break;
      case SAVE:
	this.saveShow();
	break;
      case EXIT:
	this.exitShow();
	break;
      case GOTO:
	this.goToSlide();
	break;
      case ABOUT:
	this.aboutShow();
	break;
      default:
	break;
    }
  }

  private void aboutShow() {
    controller.aboutShow(frame);
  }

  private void goToSlide() {
    controller.goToSlide(frame, 2);
  }

  private void exitShow() {
    controller.exitShow();
  }

  private void saveShow() {
    controller.saveShow(frame);
  }

  private void loadSlideShow() {
    controller.loadSlideShow(frame);
  }

  private void openSlideShow() {
    controller.openSlideShow(frame);
  }

  public void nextSlide() {
    controller.nextSlide(frame);
  }

  public void prevSlide() {
    controller.prevSlide(frame);
  }
}
