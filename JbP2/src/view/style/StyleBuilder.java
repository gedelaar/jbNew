package view.style;

import java.awt.Color;

import view.style.ItemStyle;

public  class StyleBuilder {

  private Color color;
  private String font;
  private int fontSize;
  private int indent;
  private String position;

  public StyleBuilder withColor(Color color) {
    this.color = color;
    return this;
  }

  public StyleBuilder withFont(String font) {
    this.font = font;
    return this;
  }

  public StyleBuilder withFontSize(int i) {
    this.fontSize = i;
    return this;
  }

  public StyleBuilder withIndent(int i) {
    this.indent = i;
    return this;
  }

  public StyleBuilder withPosition(String position) {
    this.position = position;
    return this;
  }

  public ItemStyle build() {
    return new ItemStyle(this);
  }

  public Color getColor() {
    return color;
  }

  public String getFont() {
    return font;
  }

  public int getFontSize() {
    return fontSize;
  }

  public int getIndent() {
    return indent;
  }

  public String getPosition() {
    return position;
  }
}