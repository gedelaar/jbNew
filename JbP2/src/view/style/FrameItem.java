package view.style;

public abstract class FrameItem<E> {

	private StyleBuilder style;

	public StyleBuilder getStyle() {
		return style;
	}

	public void setStyle(StyleBuilder style) {
		this.style = style;
	}
	
	public abstract E getLine();
}
