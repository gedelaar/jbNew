package view.style;

import java.awt.Color;

public interface StyleInterface {

	public Color getColor();

	public String getFont();

	public int getFontSize();

	public int getIndent();
	
	public String getPosition();

}
