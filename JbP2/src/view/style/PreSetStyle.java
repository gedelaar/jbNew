package view.style;

import java.awt.Color;
import java.awt.Font;

import view.style.StyleBuilder;

public class PreSetStyle {

  public static final String ZERO = "0";
  public static final String ONE = "1";
  public static final String TWO = "2";
  public static final String THREE = "3";
  public static final String FOUR = "4";

  public static final Integer MIN = Integer.parseInt(ZERO);
  public static final Integer MAX = Integer.parseInt(FOUR);
  public static final String HEADER = "header";

  int factor = 35;

  public StyleBuilder getStyle(String type) {
    switch (type) {
      case ZERO:
	return new StyleBuilder().withColor(Color.RED).withFont(Font.SANS_SERIF).withFontSize(35);

      case ONE:
	return new StyleBuilder().withColor(Color.BLUE).withFont(Font.SANS_SERIF).withFontSize(30)
	    .withIndent(1 * factor);
      case TWO:
	return new StyleBuilder().withColor(Color.BLACK).withFont(Font.SANS_SERIF).withFontSize(25)
	    .withIndent(2 * factor);
      case THREE:
	return new StyleBuilder().withColor(Color.BLACK).withFont(Font.SANS_SERIF).withFontSize(20)
	    .withIndent(3 * factor);
      case FOUR:
	return new StyleBuilder().withColor(Color.BLACK).withFont(Font.SANS_SERIF).withFontSize(15)
	    .withIndent(4 * factor);
      case HEADER:
	return new StyleBuilder().withColor(Color.BLACK).withFont(Font.SANS_SERIF).withFontSize(10)
	    .withPosition("right");
      default:
    }
    return null;
  }
}
