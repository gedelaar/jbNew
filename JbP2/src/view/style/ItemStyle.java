package view.style;

import java.awt.Color;

public class ItemStyle implements StyleInterface {
  private final Color color;
  private final String font;
  private final int fontSize;
  private final int indent;
  private final String position;

  public ItemStyle(StyleBuilder itemStyleBuilder) {
    this.color = itemStyleBuilder.getColor();
    this.font = itemStyleBuilder.getFont();
    this.fontSize = itemStyleBuilder.getFontSize();
    this.indent = itemStyleBuilder.getIndent();
    this.position = itemStyleBuilder.getPosition();
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("ItemStyle [color=");
    builder.append(color);
    builder.append(", font=");
    builder.append(font);
    builder.append(", fontSize=");
    builder.append(fontSize);
    builder.append(", indent=");
    builder.append(indent);
    builder.append(", position=");
    builder.append(position);
    builder.append("]");
    return builder.toString();
  }

  @Override
  public Color getColor() {
    return color;
  }

  @Override
  public String getFont() {
    return font;
  }

  @Override
  public int getFontSize() {
    return fontSize;
  }

  @Override
  public int getIndent() {
    return indent;
  }

  @Override
  public String getPosition() {
    return position;
  }

 
}
