package view.style;

public class Image extends FrameItem<String> {
	private final String image;

	public Image(final String image) {
		this.image = image;
	}

	@Override
	public String getLine() {
		return image;
	}

}
