package controller;

import javax.swing.JMenuBar;

public class EnumMenu {
  public enum MenuItem {
    OPEN("Open"), NEXT("Next"), PREV("Prev"), NEW("New"), SAVE("Save"), FILE("File"), EXIT("Exit"), VIEW("View"), GOTO(
	"GoTo"), HELP("Help"), ABOUT("About");

    public final String menuTextItem;
    public final JMenuBar menuBarItem;

    private MenuItem(String menuItem) {
      this.menuBarItem = menuItem();
      this.menuTextItem = menuItem;
    }

    public JMenuBar menuItem() {
      JMenuBar jMenuBar = new JMenuBar();
      jMenuBar.add(new JMenuBar());
      return jMenuBar;
    }
  }
}