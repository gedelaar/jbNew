package controller.jabberpoint;

import controller.Presentation;

public class JabberPoint implements ShowPresentationFacade {

  public static void main(String[] args) {
    showPresentation();
  }

  private static void showPresentation() {
    new Presentation().showPresentation();
  }

}
