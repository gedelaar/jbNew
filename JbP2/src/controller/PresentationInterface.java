package controller;

import model.slideshow.SlideShow;
import model.slideshow.SlideShowIterator;
import view.frame.SliderFrame;

public interface PresentationInterface {
  public void openSlideShow(SliderFrame frame);

  public void nextSlide(SliderFrame frame);

  public void prevSlide(SliderFrame frame);

  public void goToSlide(SliderFrame frame, int index);

  public void loadSlideShow(SliderFrame frame);

  public void saveShow(SliderFrame frame);
  
  public void exitShow();
  
  public void aboutShow(SliderFrame frame);

  public SlideShowIterator<SlideShow> getSlideShowIterator();
}