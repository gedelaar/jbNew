package controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import model.item.ImageItem;
import model.item.SlideItem;
import model.item.type.Attribute;
import model.item.type.LevelType;
import model.slide.Slide;
import model.slide.SlideIterator;
import model.slideshow.SlideShow;
import model.slideshow.SlideShowFactory;
import model.slideshow.SlideShowIterator;
import model.slideshow.SlideShowSave;
import model.slideshow.meta.MetaInformation;
import model.slideshow.meta.Title;
import view.frame.AboutBox;
import view.frame.SliderFrame;
import view.style.PreSetStyle;
import view.style.StyleBuilder;

/**
 * @author gerard/marcel
 * 
 *         controller of the presentation
 *         contains all of the operations that can be performed on the slideshow
 *
 */
public class Presentation implements PresentationInterface {
  private SlideShow slideShow;
  private Iterator<?> slideShowIterator;
  private SlideShowFactory slideShowFactory;

  public SlideShowIterator<SlideShow> getSlideShowIterator() {
    return ((SlideShowIterator<SlideShow>) slideShowIterator);
  }

  public void showPresentation() {
    SliderFrame frame = new SliderFrame(this);
    this.loadSlideShow(frame);
  }

  @Override
  public void openSlideShow(SliderFrame frame) {
    slideShowFactory = new SlideShowFactory();
    this.slideShow = slideShowFactory.create("simple");
    frame.setTitle(getMetaTitle());
    slideShowIterator = (SlideShowIterator<?>) this.slideShow.iterator();
    this.nextSlide(frame);
  }

  @Override
  public void loadSlideShow(SliderFrame frame) {
    slideShowFactory = new SlideShowFactory();
    this.slideShow = slideShowFactory.create("demo");
    frame.setTitle(getMetaTitle());
    slideShowIterator = (SlideShowIterator<?>) this.slideShow.iterator();
    this.nextSlide(frame);
  }

  @Override
  public void nextSlide(SliderFrame frame) {
    if (slideShowIterator != null && slideShowIterator.hasNext()) {
      Slide slideIt = (Slide) slideShowIterator.next();
      this.getFrameText(frame, (SlideIterator<?>) slideIt.iterator());
    }
  }

  @Override
  public void prevSlide(SliderFrame frame) {
    if (slideShowIterator != null && ((SlideShowIterator<?>) slideShowIterator).hasPrev()) {
      Slide slideIt = (Slide) ((SlideShowIterator<?>) slideShowIterator).prev();
      this.getFrameText(frame, (SlideIterator<?>) slideIt.iterator());
    }
  }

  @Override
  public void goToSlide(SliderFrame frame, int index) {
    if (slideShowIterator != null) {
      Slide slideIt = (Slide) ((SlideShowIterator<?>) slideShowIterator).getSlide(index);
      this.getFrameText(frame, (SlideIterator<?>) slideIt.iterator());
    }
  }

  @Override
  public void saveShow(SliderFrame frame) {
    if (slideShow != null) {
      SlideShowSave slideShowSave = new SlideShowSave();
      try {
	slideShowSave.saveSlideShow(slideShow);
      }
      catch (ParserConfigurationException e) {
	e.printStackTrace();
      }
    }
  }

  @Override
  public void exitShow() {
    System.exit(0);
  }

  @Override
  public void aboutShow(SliderFrame frame) {
    AboutBox.show(frame);
  }

  private void getFrameText(SliderFrame frame, SlideIterator<?> slideItItem) {
    List<SlideItem> slideItems = new ArrayList<>();
    while (slideItItem.hasNext()) {
      SlideItem slideItem = (SlideItem) slideItItem.next();
      if (this.isImageItem(slideItem)) {
	((ImageItem) slideItem).getImage().setStyle(getStyle(getLevelValue(slideItem.attributes())));
      }
      slideItem.getLine().setStyle(getStyle(getLevelValue(slideItem.attributes())));
      slideItems.add(slideItem);
    }
    frame.addPaneList(slideItems);
  }

  private String getLevelValue(List<Attribute> attributes) {
    for (Attribute attr : attributes) {
      if (attr.getValue().equalsIgnoreCase(LevelType.LEVEL)) {
	return attr.getAttributeValue().getValue();
      }
    }
    return PreSetStyle.ZERO;
  }

  private StyleBuilder getStyle(String type) {
    return new PreSetStyle().getStyle(type);
  }

  private String getMetaTitle() {
    for (MetaInformation meta : slideShow.metaInformation()) {
      if (meta.getMetaType().equals(Title.SHOWTITLE)) {
	return meta.getValue();
      }
    }
    return null;
  }

  private boolean isImageItem(SlideItem slideItem) {
    return slideItem instanceof ImageItem;
  }
}