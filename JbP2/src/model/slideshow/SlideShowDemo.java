package model.slideshow;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import exception.KindTypeException;
import io.xml.mapper.SlideItemMapper;
import io.xml.mapper.SlideMapper;
import io.xml.xmlenum.XmlType.XmlTypes;
import model.item.SlideItem;
import model.item.type.Attribute;
import model.item.type.KindType;
import model.item.type.LevelType;
import model.slide.Slide;
import model.slideshow.meta.MetaInformation;
import model.slideshow.meta.Presenter;
import model.slideshow.meta.SubTitle;
import model.slideshow.meta.Title;
import view.style.Line;

/**
 * Een ingebouwde demo-presentatie
 * 
 * @author Ian F. Darwin, ian@darwinsys.com, Gert Florijn, Sylvia Stuurman
 * @version 1.1 2002/12/17 Gert Florijn
 * @version 1.2 2003/11/19 Sylvia Stuurman
 * @version 1.3 2004/08/17 Sylvia Stuurman
 * @version 1.4 2007/07/16 Sylvia Stuurman
 * @version 1.5 2010/03/03 Sylvia Stuurman
 * @version 1.6 2014/05/16 Sylvia Stuurman
 * @version 1.7 2020/03/01 Gerard Edelaar
 */

public class SlideShowDemo implements SlideShow {

  public SlideShow open(SlideShow slideShow) throws KindTypeException {
    final String title = XmlTypes.TITLE.toString().toLowerCase();
    final String item =  XmlTypes.ITEM.toString().toLowerCase();
    
    List<SlideItem> items = new ArrayList<>();
    slideShow.metaInformation().add(new Title("Demo Presentation"));
    slideShow.metaInformation().add(new SubTitle("Demo subPresentation"));
    slideShow.metaInformation().add(new Presenter("Marcel Claus and Gerard Edelaar"));
    
    items.add(createItem(title, "JabberPoint", null, null));
    items.add(createItem(item, "Het Java Presentatie Tool", "text", "1"));
    items.add(createItem(item, "Copyright (c) 1996-2000: Ian Darwin", "text", "2"));
    items.add(createItem(item, "Copyright (c) 2000-now:", "text", "2"));
    items.add(createItem(item, "Gert Florijn en Sylvia Stuurman", "text", "2"));
    items.add(createItem(item, "JabberPoint aanroepen zonder bestandsnaam", "text", "4"));
    items.add(createItem(item, "laat deze presentatie zien", "text", "4"));
    items.add(createItem(item, "Navigeren:", "text", "1"));
    items.add(createItem(item, "Volgende slide: PgDn of Enter", "text", "3"));
    items.add(createItem(item, "Vorige slide: PgUp of up-arrow", "text", "3"));
    items.add(createItem(item, "Stoppen: q or Q", "text", "3"));
    slideShow.slides().add((new SlideMapper()).handleSlide(items));
    items = createSlide();

    // next slide
    items.add(createItem(title, "Demonstratie van levels en stijlen", null, null));
    items.add(createItem(item, "Level 1", "text", "1"));
    items.add(createItem(item, "Level 2", "text", "2"));
    items.add(createItem(item, "Nogmaals level 1", "text", "1"));
    items.add(createItem(item, "Level 1 heeft stijl nummer 1", "text", "1"));
    items.add(createItem(item, "Level 2 heeft stijl nummer 2", "text", "2"));
    items.add(createItem(item, "Zo ziet level 3 er uit", "text", "3"));
    items.add(createItem(item, "En dit is level 4", "text", "4"));
    slideShow.slides().add((new SlideMapper()).handleSlide(items));
    items = createSlide();

    // next slide
    items.add(createItem(title, "De derde slide", null, null));
    items.add(createItem(item, "Om een nieuwe presentatie te openen,", "text", "1"));
    items.add(createItem(item, "gebruik File->Open uit het menu.", "text", "2"));
    items.add(createItem(item, " ", "text", "1"));
    items.add(createItem(item, "Dit is het einde van de presentatie.", "text", "1"));
    items.add(createItem(item, "JabberPoint.jpg", "image", "1"));
    slideShow.slides().add((new SlideMapper()).handleSlide(items));
    items = createSlide();
    return slideShow;

  }

  private SlideItem createItem(String node, String strline, String kindAttr, String levelAttr) throws KindTypeException {
    Line line = new Line(strline);
    SlideItemMapper slideItemMapper = new SlideItemMapper();
    if (null != kindAttr || null != levelAttr) {
      List<Attribute> attributes = new ArrayList<>();
      attributes.add(new KindType(kindAttr));
      attributes.add(new LevelType(levelAttr));
      return slideItemMapper.setItemValues(node, line, attributes);
    }
    return slideItemMapper.setItemValues(node, line, null);
  }

  private List<SlideItem> createSlide() {
    return new ArrayList<>();
  }

  @Override
  public Iterator<Slide> iterator() {
    return null;
  }

  @Override
  public List<Slide> slides() {
    return null;
  }

  @Override
  public List<MetaInformation> metaInformation() {
     return null;
  }
}
