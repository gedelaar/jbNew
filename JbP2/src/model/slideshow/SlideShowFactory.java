package model.slideshow;

import java.io.IOException;

import org.xml.sax.SAXException;

import exception.KindTypeException;
import io.xml.wrapper.XmlSlideShowWrapper;
import model.AbstractFactory;

public class SlideShowFactory implements AbstractFactory<SlideShow> {

  @Override
  public SlideShow create(String type)  {
    if (type.equals("simple")) {
      return createSimple();
    }
    if (type.equals("demo")) {
      try {
	return createDemo();
      }
      catch (KindTypeException e) {
	e.printStackTrace();
      }
    }
    return null;
  }

  private SlideShow createSimple() {
    try {
      return new XmlSlideShowWrapper().slideShowWrapper(new SimpleSlideShow());
    }
    catch (SAXException e) {
      e.printStackTrace();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  private SlideShow createDemo() throws KindTypeException {
    return new SlideShowDemo().open(new SimpleSlideShow());
  }
}
