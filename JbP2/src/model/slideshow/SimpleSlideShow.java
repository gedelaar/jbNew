package model.slideshow;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import model.slide.Slide;
import model.slideshow.meta.MetaInformation;

public class SimpleSlideShow implements SlideShow {
  private List<MetaInformation> metaInformations = new ArrayList<>();
  private List<Slide> slideList = new ArrayList<>();
  private SlideShowIterator<Slide> slideShowIterator;

  @Override
  public List<Slide> slides() {
    return slideList;
  }

  @Override
  public List<MetaInformation> metaInformation() {
    return metaInformations;
  }

  public int getMetaIndex(String value) {
    return metaInformations.indexOf(value);
  }

  @Override
  public Iterator<Slide> iterator() {
    this.slideShowIterator = new SlideShowIterator<>(this.slides());
    return this.slideShowIterator;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("SimpleSlideShow [metaInformations=");
    builder.append(metaInformations);
    builder.append(", slides=");
    builder.append(slideList);
    builder.append(", slideShowIterator=");
    builder.append(slideShowIterator);
    builder.append("]");
    return builder.toString();
  }

}
