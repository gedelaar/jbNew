package model.slideshow.meta;

public interface MetaInformation {

  public String getValue();

  public String getMetaType();

}
