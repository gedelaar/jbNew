package model.slideshow.meta;

public class Presenter implements MetaInformation {
  private static final String PRESENTER = "presenter";
  private String presenterName;

  public Presenter(String presenter) {
    super();
    this.presenterName = presenter;
  }

  @Override
  public String getValue() {
    return this.presenterName;
  }

  @Override
  public String getMetaType() {
    return PRESENTER;
  }

}
