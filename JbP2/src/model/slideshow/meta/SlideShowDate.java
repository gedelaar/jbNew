package model.slideshow.meta;

import model.validator.DateValidator;

public class SlideShowDate implements MetaInformation {
  private static final String SLIDESHOWDATE = "slideshowdate";
  private String date;

  public SlideShowDate(String strDate) {
    super();
    DateValidator dateValidator = new DateValidator();
    if (dateValidator.isThisDateValid(strDate, "dd-mm-yyyy")) {
      this.date = strDate;
    }
  }

  @Override
  public String getValue() {
    return date;
  }

  @Override
  public String getMetaType() {
    return SLIDESHOWDATE;
  }

}
