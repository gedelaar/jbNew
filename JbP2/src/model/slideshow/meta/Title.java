package model.slideshow.meta;

import view.style.Line;

public  class Title implements MetaInformation {
  public static final String SHOWTITLE = "showtitle";
  private Line title;

  public Title(String title) {
    super();
    this.title = new Line(title);
  }

  @Override
  public String getValue() {
    return this.title.getStrLine();
  }

  @Override
  public String getMetaType() {
    return SHOWTITLE;
  }

}
