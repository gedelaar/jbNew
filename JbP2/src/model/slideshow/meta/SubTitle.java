package model.slideshow.meta;

import view.style.Line;

public class SubTitle implements MetaInformation {
  private static final String SUBTITLE = "subtitle";
  private Line subTitle;

  public SubTitle(String subTitle) {
    super();
    this.subTitle = new Line(subTitle);
  }

  @Override
  public String getValue() {
    return subTitle.getStrLine();
  }

  @Override
  public String getMetaType() {
    return SUBTITLE;
  }

}
