package model.slideshow;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import model.item.SlideItem;
import model.item.type.Attribute;
import model.slide.Slide;
import model.slideshow.meta.MetaInformation;

public class SlideShowSave {
  // ombouwen tot builder pattern
  public void saveSlideShow(SlideShow slideShow) throws ParserConfigurationException {

    DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

    // root elements
    Document doc = docBuilder.newDocument();
    Element rootElement = doc.createElement("presentation");
    doc.appendChild(rootElement);

    // presentation elements
    for (MetaInformation ms : slideShow.metaInformation()) {
      Element showMetaInfo = doc.createElement(ms.getMetaType());
      rootElement.appendChild(showMetaInfo);
      showMetaInfo.appendChild(doc.createTextNode(ms.getValue()));

    }

    // slide elements
    for (Slide slide : slideShow.slides()) {
      Element slideElement = doc.createElement("slide");
      rootElement.appendChild(slideElement);
      for (SlideItem item : slide.items()) {
	Element nodeItem = doc.createElement(item.getNaam());
	slideElement.appendChild(nodeItem);
	nodeItem.appendChild(doc.createTextNode(item.getLine().getStrLine()));
	if (null != item.attributes() && !item.attributes().isEmpty()) {
	  for (Attribute attribute : item.attributes()) {
	    Attr attr = doc.createAttribute(attribute.getValue());
	    attr.setValue(attribute.getAttributeValue().getValue());
	    nodeItem.setAttributeNode(attr);
	  }
	}
      }
    }

    Transformer transformer;
    try {
      transformer = TransformerFactory.newInstance().newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      DOMSource source = new DOMSource(doc);
      StreamResult console = new StreamResult(System.out);
      try {
	transformer.transform(source, console);
      }
      catch (TransformerException e) {
	e.printStackTrace();
      }
    }
    catch (TransformerConfigurationException e) {
      e.printStackTrace();
    }
    catch (TransformerFactoryConfigurationError e) {
      e.printStackTrace();
    }
  }
}
