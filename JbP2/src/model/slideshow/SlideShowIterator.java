package model.slideshow;

import java.util.Iterator;
import java.util.List;

import model.slide.Slide;

public class SlideShowIterator<E> implements Iterator<E> {
  private List<Slide> slides;
  private int position = -1;

public SlideShowIterator(List<Slide> slides) {
    this.slides = slides;
  }

  @Override
  public boolean hasNext() {
    return (position < slides.size() - 1);
  }

  public boolean hasPrev() {
    return (position > 0 && position < slides.size());
  }

  @Override
  public E next() {
    if (this.hasNext()) {
      position++;
      Slide slide = slides.get(position);
      return (E) slide;
    }
    return null;
  }

  public E prev() {
    if (hasPrev()) {
      position--;
      Slide slide = slides.get(position);
      return (E) slide;
    }
    return null;
  }

  public E getSlide(int index) {
    if (index < 0) {
      index = 0;
    }
    for (int i = 0; (i == index - 1 || i < slides.size()); i++) {
      if (index == i) {
	position = i - 1;
	return (E) slides.get(position);
      }
    }
    return null;
  }
  
  public int getCurrentSlideNumber() {
	return position+1;
}
  public int getMaxSlideNumber() {
	  return slides.size();
  }
}
