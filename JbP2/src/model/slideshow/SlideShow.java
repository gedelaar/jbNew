package model.slideshow;

import java.util.Iterator;
import java.util.List;

import model.slide.Slide;
import model.slideshow.meta.MetaInformation;

public interface SlideShow extends Iterable<Slide> {

  public List<Slide> slides();

  public List<MetaInformation> metaInformation();
  
  public Iterator<Slide> iterator();


}
