package model;

public interface AbstractFactory<E> {

  public E create(String type);

}
