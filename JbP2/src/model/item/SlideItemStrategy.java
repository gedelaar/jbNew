package model.item;

public abstract class SlideItemStrategy implements SlideItem {

  public abstract SlideItem getSlideItem();
  
}
