package model.item;

import java.util.ArrayList;
import java.util.List;

import model.item.type.Attribute;
import view.style.Line;

public class ImageItem extends SlideItemStrategy implements SlideItem {
  private Line line;
  private String naam;
  private view.style.Image image;
  private List<Attribute> attributes = new ArrayList<>();

  @Override
  public Line getLine() {
    return this.line;
  }

  @Override
  public void setLine(Line line) {
    this.line = line;
  }

  @Override
  public void setNaam(String naam) {
    this.naam = naam;
  }

  @Override
  public String getNaam() {
    return this.naam;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("ImageItem [line=");
    builder.append(line);
    builder.append(", naam=");
    builder.append(naam);
    builder.append(", image=");
    builder.append(image);
    builder.append(", attributes=");
    builder.append(attributes);
    builder.append("]");
    return builder.toString();
  }

  public view.style.Image getImage() {
    return image;
  }

  public void setImage(view.style.Image image) {
    this.image = image;
  }

  public List<Attribute> attributes() {
    return attributes;
  }

  @Override
  public SlideItem getSlideItem() {
    return this;
  }

}
