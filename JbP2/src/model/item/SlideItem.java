package model.item;

import java.util.List;

import model.item.type.Attribute;
import view.style.Line;

public interface SlideItem  {

  public void setLine(Line line);

  public Line getLine();

  public void setNaam(String naam);

  public String getNaam();

  public List<Attribute> attributes();

}
