package model.item;

import java.util.List;

import model.item.type.Attribute;
import view.style.Line;

public class TableItem extends SlideItemStrategy {

  @Override
  public void setLine(Line line) {
  }

  @Override
  public Line getLine() {
    return null;
  }

  @Override
  public void setNaam(String naam) {
  }

  @Override
  public String getNaam() {
    return null;
  }

  @Override
  public List<Attribute> attributes() {
    return null;
  }

  @Override
  public SlideItem getSlideItem() {
    return this;
  }

}
