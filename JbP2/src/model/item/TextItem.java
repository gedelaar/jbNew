package model.item;

import java.util.ArrayList;
import java.util.List;

import model.item.type.Attribute;
import model.item.type.text.ListTextItem;
import view.style.Line;

public class TextItem extends SlideItemStrategy implements SlideItem {
  private Line line;
  private List<ListTextItem> listTextItems;
  private String naam;
  private List<Attribute> attributes = new ArrayList<>();

  public TextItem() {
  }

  @Override
  public Line getLine() {
    return line;
  }

  @Override
  public void setLine(Line line) {
    this.line = line;
  }

  @Override
  public String getNaam() {
    return this.naam;
  }

  @Override
  public void setNaam(String naam) {
    this.naam = naam;
  }

  @Override
  public List<Attribute> attributes() {
    return attributes;
  }

  @Override
  public SlideItem getSlideItem() {
    return this;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("TextItem [line=");
    builder.append(line);
    builder.append(", listTextItems=");
    builder.append(listTextItems);
    builder.append(", naam=");
    builder.append(naam);
    builder.append(", attributes=");
    builder.append(attributes);
    builder.append("]");
    return builder.toString();
  }
}
