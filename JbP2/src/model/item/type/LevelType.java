package model.item.type;

public class LevelType implements Attribute {
  public static final String LEVEL = "level";

  TypeLevelData levelValue;

  public LevelType(String value) {
    levelValue = new TypeLevelData(value);
  }

  @Override
  public String getValue() {
    return LEVEL;
  }

  @Override
  public AttributeValue getAttributeValue() {
    return levelValue;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Level [levelValue=");
    builder.append(levelValue);
    builder.append("]");
    return builder.toString();
  }

}
