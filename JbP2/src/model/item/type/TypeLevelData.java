package model.item.type;

import org.apache.commons.lang3.Validate;

import model.item.SlideItem;
import view.style.PreSetStyle;

/***
 * 
 * @author gerard
 *
 */
class TypeLevelData implements AttributeValue {
  private String value;

  TypeLevelData() {
    super();
  }

  TypeLevelData(String value) {
    super();
    checkValue(value);
    this.value = value;
  }

  private void checkValue(String value) {
    int i = Integer.parseInt(value);
    Validate.inclusiveBetween(PreSetStyle.MIN, PreSetStyle.MAX, i);
  }

  @Override
  public String getValue() {
    return this.value;
  }

  /*
   * not used in this context
   */
  @Override
  public SlideItem getItemType() {
    return null;
  }

}
