package model.item.type;

import model.item.SlideItem;
import model.item.TextItem;

public class TypeTextData implements AttributeValue {
  private String value;

  public TypeTextData(String value) {
    this.value = value;
  }

  @Override
  public String getValue() {
    return value;
  }
  
  @Override
  public SlideItem getItemType() {
    return new TextItem();
  }
      

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("TypeTextData [value=");
    builder.append(value);
    builder.append("]");
    return builder.toString();
  }

 
}