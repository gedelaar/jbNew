package model.item.type;

import model.item.ImageItem;
import model.item.SlideItem;

/**
 * @author gerard/marcel
 * 
 *         handels imagedefinition. todo: add checks on file extensions
 * 
 */
public class TypeImageData implements AttributeValue {
  private String value;

  public TypeImageData(String value) {
    this.value = value;
  }

  @Override
  public String getValue() {
    return value;
  }

  @Override
  public SlideItem getItemType() {
    return new ImageItem();
  }

  @Override
  public String toString() {
    return "TypeImageData [value=" + value + "]";
  }

}