package model.item.type;

import org.apache.commons.lang3.EnumUtils;

import exception.KindTypeException;
import model.item.SlideItem;

/**
 * @author gerard/marcel
 * 
 *         handles all de KIND actions
 *
 */
public class KindType implements Attribute {
  private enum KindList {
    TEXT, IMAGE;
  }

  private AttributeValue att;
  private static final String KIND = "kind";

  public static final AttributeValue TEXT = new TypeTextData("text");
  public static final AttributeValue IMAGE = new TypeImageData("image");

  private AttributeValue getAttributeValue(String value) {
    if (value.equals("text")) {
      return TEXT;
    }
    if (value.equals("image")) {
      return IMAGE;
    }
    return null;
  }

  public KindType(String value) throws KindTypeException {
    if (!checkInKindList(value)) {
      throw new KindTypeException("value " + value.toUpperCase() + " not found");
    }
    att = getAttributeValue(value);
  }

  @Override
  public String getValue() {
    return KIND;
  }

  @Override
  public AttributeValue getAttributeValue() {
    return att;
  }

  public boolean checkInKindList(String value) {
    return EnumUtils.isValidEnum(KindList.class, value.toUpperCase());
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Kind []");
    return builder.toString();
  }

}