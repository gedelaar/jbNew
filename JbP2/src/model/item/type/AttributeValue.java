package model.item.type;

import model.item.SlideItemFactory;

public interface AttributeValue extends SlideItemFactory {

  public String getValue();

}
