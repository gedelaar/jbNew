package model.item.type;

public interface Attribute {

  public String getValue();

  public AttributeValue getAttributeValue();
}
