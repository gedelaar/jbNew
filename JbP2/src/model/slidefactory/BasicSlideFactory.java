package model.slidefactory;

import model.AbstractFactory;
import model.slide.OrdinarySlide;
import model.slide.Slide;

public class BasicSlideFactory implements AbstractFactory<Slide> {

  @Override
  public Slide create(String type) {
    return new OrdinarySlide();
  }

}
