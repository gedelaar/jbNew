package model.slide;

import java.util.Iterator;
import java.util.List;

import model.item.SlideItem;

public class SlideIterator<E> implements Iterator<E> {
  private List<SlideItem> items;
  private int position;

  public SlideIterator(List<SlideItem> items) {
    this.items = items;
  }

  @Override
  public boolean hasNext() {
    return (position < items.size());
  }

  @Override
  public E next() {
    if (this.hasNext()) {
      SlideItem slideItem = items.get(position);
      position++;
      return (E) slideItem;
    }
    return null;
  }
}
