package model.slide;

import java.util.Iterator;

import model.item.SlideItem;

public class OrdinarySlide extends Slide {

  public OrdinarySlide() {
    super();
  }

  @Override
  public Iterator<SlideItem> iterator() {
    return new SlideIterator<>(this.items());
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("OrdinarySlide [items()=");
    builder.append(items());
    builder.append("]");
    return builder.toString();
  }

}
