package exception;

public class KindTypeException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  
  public KindTypeException() {}
  
  public KindTypeException(String message) {
    super(message);
  }

}
