package test.item.type;

import static org.junit.jupiter.api.Assertions.*;

import model.item.type.AttributeValue;
import model.item.type.KindType;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import exception.KindTypeException;

class KindTest {

  @BeforeAll
  static void setUpBeforeClass() throws Exception {
  }

  @BeforeEach
  void setUp() throws Exception {
  }

  @Test
  void testKind() throws KindTypeException {
    AttributeValue kindAttr = KindType.IMAGE;
    KindType kind = new KindType(kindAttr.getValue().toString().toLowerCase());
    assertEquals("kind", kind.getValue());
  }

  @Test
  void testKindFalse() throws KindTypeException {
    assertThrows(KindTypeException.class,() -> {
      String kindAttr = "NOT";
      KindType kind = new KindType(kindAttr.toString().toLowerCase());
    });

  }

  @Test
  void testImageAttribute() throws KindTypeException {
    String kindAttr = "image";
    KindType kind = new KindType(kindAttr);
    assertEquals(kindAttr, kind.getAttributeValue().getValue());
  }

  @Test
  void testTextAttribute() throws KindTypeException {
    String kindAttr = "text";
    KindType kind = new KindType(kindAttr);
    assertEquals(kindAttr, kind.getAttributeValue().getValue());
  }
  
  @Test
  void testTextSlideItem() throws KindTypeException {
    String kindAttr = "text";
    KindType kind = new KindType(kindAttr);
    //System.out.println(kind.getSlideItem());
    
    
  }
  
}
