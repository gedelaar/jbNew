package test.item.type;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.item.type.LevelType;
import view.style.PreSetStyle;

class LevelTest {

  @BeforeAll
  static void setUpBeforeClass() throws Exception {
  }

  @BeforeEach
  void setUp() throws Exception {
  }

  @Test
  void testCreate() {
    LevelType level = new LevelType("1");
    assertEquals("1", level.getAttributeValue().getValue());
    assertEquals(LevelType.LEVEL, level.getValue());
  }

  @Test
  void testGoodValues() {
    LevelType level1 = new LevelType(PreSetStyle.MIN.toString());
    assertEquals("0", level1.getAttributeValue().getValue());

    LevelType level2 = new LevelType(PreSetStyle.MAX.toString());
    assertEquals("4", level2.getAttributeValue().getValue());
  }

  @Test
  void testNotGoodValues() {
    assertThrows(IllegalArgumentException.class, () -> {
      LevelType level1 = new LevelType("8");
    });

  }

}
