package test.item;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import exception.KindTypeException;
import model.item.ImageItem;
import model.item.SlideItem;
import model.item.TextItem;
import model.item.type.AttributeValue;
import model.item.type.TypeTextData;
import view.style.Line;

class TextItemTest {

  @BeforeAll
  static void setUpBeforeClass() throws Exception {
  }

  @BeforeEach
  void setUp() throws Exception {
  }

  @Test
  void InterfaceTest() {
    String strLine = "tekst";
    SlideItem item = new TextItem();
    assertNull(strLine, item.getLine());
  }

  @Test
  void testLineSet() {
    String strLine = "tekst";
    TextItem textItem = new TextItem();
    textItem.setLine(new Line(strLine));
    assertEquals("tekst", textItem.getLine().getStrLine());
  }

  @Test
  void testAttrTextSet() throws KindTypeException {
    AttributeValue att = new TypeTextData("text");
    assertEquals("text", att.getValue());
    assertTrue(att.getItemType() instanceof TextItem);
    System.out.println(att.getItemType());
  }

  @Test
  void testAttrNotTextSet() throws KindTypeException {
    String text = "image";
    AttributeValue att = new TypeTextData(text);
    assertEquals(text, att.getValue());
    assertFalse(att.getItemType() instanceof ImageItem);
    System.out.println(att.getItemType());
  }

}
