package test.item;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import exception.KindTypeException;
import model.item.ImageItem;
import model.item.SlideItem;
import model.item.TextItem;
import model.item.type.AttributeValue;
import model.item.type.TypeImageData;
import view.style.Line;

class ImageItemTest {

  @BeforeAll
  static void setUpBeforeClass() throws Exception {
  }

  @BeforeEach
  void setUp() throws Exception {
  }

  @Test
  void InterfaceTest() {
    String strLine = "image";
    SlideItem item = new ImageItem();
    assertNull(strLine, item.getLine());
  }

  @Test
  void testLineSet() {
    String strLine = "image.jpg";
    ImageItem item = new ImageItem();
    item.setLine(new Line(strLine));
    assertEquals(strLine, item.getLine().getStrLine());
  }

  @Test
  void testAttrImageSet() throws KindTypeException {
    AttributeValue att = new TypeImageData("image");
    assertEquals("image", att.getValue());
    assertTrue(att.getItemType() instanceof ImageItem);
    System.out.println(att.getItemType());
  }

  @Test
  void testAttrNotImagetSet() throws KindTypeException {
    String text = "text";
    AttributeValue att = new TypeImageData(text);
    assertEquals(text, att.getValue());
    assertFalse(att.getItemType() instanceof TextItem);
    System.out.println(att.getItemType());
  }

}
