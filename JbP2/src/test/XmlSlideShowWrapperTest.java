package test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import io.xml.ConvertToXmlDocument;
import io.xml.wrapper.XmlSlideWrapper;
import model.slide.Slide;

class XmlSlideShowWrapperTest {
//@formatter:off
  static final String xmlStr = 
        "<?xml version=\"1.0\"?>" 
      + "<!DOCTYPE presentation SYSTEM \"jabberpoint.dtd\">"
      + "<presentation>" 
      + "     <showtitle>XML-Based Presentation for Jabberpoint</showtitle>" 
      + "     <slide>"
      + "          <title>title for Jabberpoint2</title>" 
      + "          <item>item1 for Jabberpoint2</item>"
      + "          <item kind=\"text\"  level=\"1\">item2 for Jabberpoint3</item>" 
      + "     </slide>" 
      + "</presentation>";
//@formatter:on
  XmlSlideWrapper node = new XmlSlideWrapper();

  @BeforeAll
  static void setUpBeforeClass() throws Exception {
  }

  @BeforeEach
  void setUp() throws Exception {
  }

  @Test
  void testReadXMLShowTitle() throws SAXException, IOException {
    Document doc = ConvertToXmlDocument.convertStringToXMLDocument(xmlStr);
    XmlSlideWrapper xmlSlWrap = new XmlSlideWrapper();
    Node showTitle = doc.getDocumentElement().getChildNodes().item(1);
    
    Slide slide = xmlSlWrap.handleXmlSlideWrapper((Element) showTitle);
    System.out.println(slide.items());
    System.out.println(showTitle.getNodeName());
    assertEquals("showtitle", showTitle.getNodeName());
    }

  @Test
  void testXMLSlide() throws SAXException, IOException {
    Document doc = ConvertToXmlDocument.convertStringToXMLDocument(xmlStr);
    XmlSlideWrapper xmlSlWrap = new XmlSlideWrapper();
    Node xmlSlide = doc.getDocumentElement().getChildNodes().item(2).getNextSibling();
    
    Slide slide = xmlSlWrap.handleXmlSlideWrapper((Element) xmlSlide);
    System.out.println(slide);
    System.out.println(xmlSlide.getNodeName());
    assertEquals("slide", xmlSlide.getNodeName());
    }

   
  

}
