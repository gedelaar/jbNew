package test.slideshow;

import model.item.SlideItem;
import model.item.type.Attribute;
import model.slide.Slide;
import model.slideshow.SimpleSlideShow;

public class CheckSlideShow {
  
  public  CheckSlideShow(SimpleSlideShow slideShow) {
    System.out.println("slideshow: " + slideShow.toString());
    for (Slide slide : slideShow.slides()) {
      System.out.println("");
      for (SlideItem item : slide.items()) {
	System.out.println("    slideItems: " + item);
	System.out.println("    Item Line: " + item.getLine().getLine());
	if (null != item.attributes()) {
	  for (Attribute att : item.attributes()) {
	    System.out.println("      Attribute " + att.getValue() + " -> " + att.getAttributeValue().getValue());
	  }
	  System.out.println("-----------");
	}
      }
    }
  }
}
