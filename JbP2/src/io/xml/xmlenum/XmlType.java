package io.xml.xmlenum;

public class XmlType {
  public enum XmlTypes {
    PRESENTATION, SHOWTITLE, SLIDE, TITLE, ITEM, DATE, SHOWSUBTITLE, PRESENTER;

  }
}
