package io.xml.wrapper;

import org.apache.commons.lang3.EnumUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import io.xml.mapper.SlideMapper;
import io.xml.xmlenum.XmlType.XmlTypes;
import model.slide.Slide;

/**
 * @author gerard/marcel
 * 
 *         handles items in the xml-file
 *
 */
public class XmlSlideWrapper {

  public Slide handleXmlSlideWrapper(final Element element) {
    SlideMapper slideMapper = new SlideMapper();
    Slide slide = slideMapper.getSlideAbstractFactory();
    this.addItems(element, slide);
    return slide;
  }

  private void addItems(final Element e, Slide slide) {
    final NodeList children = e.getChildNodes();

    for (int i = 0; i < children.getLength(); i++) {
      final Node n = children.item(i);
      if (n.getNodeType() == Node.ELEMENT_NODE) {
	EnumUtils.isValidEnum(XmlTypes.class, n.getNodeName().toUpperCase());
	slide.items().add((new XmlItemWrapper()).itemValueXMLWrapper(n));

	addItems((Element) n, slide);
      }
    }
  }
}
