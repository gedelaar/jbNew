package io.xml.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import model.item.type.Attribute;
import model.item.type.KindType;
import model.item.type.LevelType;

public class XmlItemAttributeWrapper {
  List<Attribute> attributes = new ArrayList<>();
  Attribute kind;
  Attribute level;

  public List<Attribute> AttributeXMLWrapper(Node tempNode) {
    NamedNodeMap nodeMap = tempNode.getAttributes();

    for (int j = 0; j < nodeMap.getLength(); j++) {
      Node node = nodeMap.item(j);

      try {
	kind = new KindType(node.getNodeValue());
	if (kind.getAttributeValue() != null) {
	  attributes.add(kind);
	}
      }
      catch (Exception e) {
      }
      try {
	level = new LevelType(node.getNodeValue());
	attributes.add(level);
      }
      catch (Exception e) {
      }
    }
    return attributes;
  }

}
