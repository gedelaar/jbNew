package io.xml.wrapper;

import java.util.List;

import org.w3c.dom.Node;

import io.xml.mapper.SlideItemMapper;
import model.item.SlideItem;
import model.item.type.Attribute;
import view.style.Line;

/**
 * @author gerard/marcel
 * 
 *         handling individual attributes to a List of attributes for a slide
 *
 */
public class XmlItemWrapper {

  public SlideItem itemValueXMLWrapper(Node node) {
    String naam = node.getNodeName();
    Line line = new Line(node.getTextContent());

    List<Attribute> attributes = null;
    XmlItemAttributeWrapper xmlItemAttribute = new XmlItemAttributeWrapper();

    if (node.hasAttributes()) {
      attributes = xmlItemAttribute.AttributeXMLWrapper(node);
    }
    SlideItemMapper slideItemMapper = new SlideItemMapper();
    return slideItemMapper.setItemValues(naam, line, attributes);
  }
}
