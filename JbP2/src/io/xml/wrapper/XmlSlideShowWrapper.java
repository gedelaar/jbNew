package io.xml.wrapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import io.xml.ConvertToXmlDocument;
import io.xml.xmlenum.XmlType.XmlTypes;
import model.slide.Slide;
import model.slideshow.SimpleSlideShow;
import model.slideshow.SlideShow;
import model.slideshow.meta.Presenter;
import model.slideshow.meta.SlideShowDate;
import model.slideshow.meta.SubTitle;
import model.slideshow.meta.Title;

public class XmlSlideShowWrapper {
  private List<Slide> slides = new ArrayList<>();

  public SimpleSlideShow slideShowWrapper(SlideShow slideShow) throws SAXException, IOException {

    Document doc = ConvertToXmlDocument.convertFileToXMLDocument();
    this.addSlide(doc.getDocumentElement(), slideShow);
    slideShow.slides().addAll(slides);
    return (SimpleSlideShow) slideShow;
  }

  private void addSlide(final Element e, SlideShow slideShow) {
    final NodeList children = e.getChildNodes();

    for (int i = 0; i < children.getLength(); i++) {
      final Node n = children.item(i);
      if (n.getNodeType() == Node.ELEMENT_NODE) {

	XmlTypes nodeName = XmlTypes.valueOf(n.getNodeName().toUpperCase());
	switch (nodeName) {
	  case SLIDE:
	    slides.add((new XmlSlideWrapper()).handleXmlSlideWrapper((Element) n));
	    break;
	  case SHOWTITLE:
	    slideShow.metaInformation().add(new Title(n.getTextContent()));
	    break;
	  case SHOWSUBTITLE:
	    slideShow.metaInformation().add(new SubTitle(n.getTextContent()));
	    break;
	  case PRESENTER:
	    slideShow.metaInformation().add(new Presenter(n.getTextContent()));
	    break;
	  case DATE:
	    slideShow.metaInformation().add(new SlideShowDate(n.getTextContent()));
	    break;

	  default:
	    break;
	}
	addSlide((Element) n, slideShow);
      }
    }
  }
}
