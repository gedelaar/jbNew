package io.xml.mapper;

import java.util.List;

import model.item.ImageItem;
import model.item.SlideItem;
import model.item.TextItem;
import model.item.type.Attribute;
import model.item.type.KindType;
import view.style.Image;
import view.style.Line;

public class SlideItemMapper {
  public SlideItem setItemValues(String naam, Line line, List<Attribute> attributes) {
    SlideItem item = null;

    item = getItem(attributes);
    if (null != attributes && !attributes.isEmpty()) {
      try {
	item.attributes().addAll(attributes);
      }
      catch (NullPointerException e) {
	System.out.println(e.getMessage());
	System.out.println(getClass() + " " + item.getLine().getLine());
      }
    }
    item.setNaam(naam);
    if (item instanceof ImageItem) {
      ((ImageItem) item).setImage(new Image(line.getStrLine()));
    }
    item.setLine(line);
    return item;
  }

  private SlideItem getItem(List<Attribute> attributes) {
    if (attributes != null) {
      for (Attribute attribute : attributes) {
	if (attribute instanceof KindType) {
	  return attribute.getAttributeValue().getItemType();
	}
      }
    }
    return new TextItem();
  }

}
