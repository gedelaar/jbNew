package io.xml.mapper;

import java.util.List;

import model.item.SlideItem;
import model.slide.Slide;
import model.slidefactory.BasicSlideFactory;

/**
 * @author gerard/marcel
 * 
 *         mapper class for input mapping to domain class
 *
 */
public class SlideMapper {
  public Slide handleSlide(List<SlideItem> items) {
    Slide slide = getSlideAbstractFactory();
    slide.items().addAll(items);
    return slide;
  }

  public Slide getSlideAbstractFactory() {
    return new BasicSlideFactory().create(null);
  }

}
