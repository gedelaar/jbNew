package io.xml;

//BaseFileParser.java
public abstract class BaseFileParser {
	public abstract void parseFile();
}
