package io.xml;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ConvertToXmlDocument {
  protected static final String FILENAME = "test.xml";

  private ConvertToXmlDocument() {

  }

  public static Document convertFileToXMLDocument() throws SAXException, IOException {
    File xmlFile = new File(FILENAME);
    return documentHandler().parse(xmlFile);
  }

  public static Document convertStringToXMLDocument(String xmlString) throws SAXException, IOException {
    return documentHandler().parse(new InputSource(new StringReader(xmlString)));

  }

  private static DocumentBuilder documentHandler() {
    DocumentBuilderFactory docbuildFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder docBuilder = null;
    try {
      docBuilder = docbuildFactory.newDocumentBuilder();
    }
    catch (ParserConfigurationException e) {
      e.printStackTrace();
    }
    return docBuilder;
  }
}
